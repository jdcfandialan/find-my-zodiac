<?php 
	session_start();
 ?>

<!DOCTYPE html>
<html>
<head>
	<title>Find my Zodiac Sign</title>
</head>
<body class="bg-primary">
	<div class="d-flex justify-content-center align-items-center flex-column vh-100">
		<h1 class="text-center py-2 text-white">Find your Zodiac Sign</h1>
		<form action="controllers/find-zodiac.php" method = "POST" class="bg-warning rounded p-5">
			<div class="form-group py-1">
				<label for="name" class="text-dark">First Name: </label>
				<input type="text" name="name" class="form-control">
			</div>

			<div class="form-group py-1">
				<label class="form-control-label" for="birthMonth" class="text-dark">Birth Month: </label>
				<input type="number" name="birthMonth" class="form-control">
			</div>

			<div class="form-group py-1">
				<label for="birthDate" class="text-dark">Birth Date: </label>
				<input type="number" name="birthDate" class="form-control">
			</div>

			<div class="text-center">
				<button type="submit" class="btn btn-success btn-outline-success" id="find-zodiac">Find my Zodiac</button>
			</div>
		</form>
	</div>

	<script type="text/javascript" src="assets/scripts/script.js"></script>
	<!-- Bootswatch -->
	<link rel="stylesheet" type="text/css" href="https://bootswatch.com/4/slate/bootstrap.css">
</body>
</html>