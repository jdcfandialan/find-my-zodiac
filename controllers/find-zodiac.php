<?php 
	session_start();
 	
 	$name = $_POST['name'];
 	$month = $_POST['birthMonth'];
 	$date = $_POST['birthDate'];



 	//cases for blank inputs or date and months exceeding 12 and 31 respectively
 	if(strlen($name) == 0 || $month <= 0 || $date <= 0 || $month == "" || $date == "" || $month > 12 || $date > 31){
 		header("Location: ".$_SERVER['HTTP_REFERER']);
 	}

 	else{
 		$_SESSION['name'] = $name;

 		//case for aquarius
 		if(($month == 1 && $date >= 20) || ($month == 2 && $date <= 18)){
 			$_SESSION['zodiac'] = "AQUARIUS";
 			echo "Aquarius";
 			header("Location: ../views/view-zodiac.php");
 		}

 		//case for input date for February exceeding 29
 		else if($month == 2 && $date >29){
 			header("Location: ".$_SERVER['HTTP_REFERER']);
 		}

 		//case for pisces
 		else if(($month == 2 && $date >= 19) || ($month == 3 && $date <= 20)){
 			$_SESSION['zodiac'] = "PISCES";
 			echo "Pisces";
 			header("Location: ../views/view-zodiac.php");
 		}

 		//case for aries
 		else if(($month == 3 && $date >= 21) || ($month == 4 && $date <= 19)){
 			$_SESSION['zodiac'] = "ARIES";
 			echo "Aries";
 			header("Location: ../views/view-zodiac.php");
 		}

 		//case for input date for April exceeding 30
 		else if($month == 4 && $date >30){
 			header("Location: ".$_SERVER['HTTP_REFERER']);
 		}

 		//case for taurus
 		else if(($month == 4 && $date >= 20) || ($month == 5 && $date <= 20)){
 			$_SESSION['zodiac'] = "TAURUS";
 			echo "Taurus";
 			header("Location: ../views/view-zodiac.php");
 		}

 		//case for gemini
 		else if(($month == 5 && $date >= 21) || ($month == 6 && $date <= 20)){
 			$_SESSION['zodiac'] = "GEMINI";
 			echo "Gemini";
 			header("Location: ../views/view-zodiac.php");
 		}

		//case for input date for June exceeding 30
 		else if($month == 6 && $date >30){
 			header("Location: ".$_SERVER['HTTP_REFERER']);
 		}

 		//case for cancer
 		else if(($month == 6 && $date >= 21) || ($month == 7 && $date <= 22)){
 			$_SESSION['zodiac'] = "CANCER";
 			echo "Cancer";
 			header("Location: ../views/view-zodiac.php");
 		}

 		//case for leo
 		else if(($month == 7 && $date >= 23) || ($month == 8 && $date <= 22)){
 			$_SESSION['zodiac'] = "LEO";
 			echo "Leo";
 			header("Location: ../views/view-zodiac.php");
 		}

 		//case for virgo
 		else if(($month == 8 && $date >= 23) || ($month == 9 && $date <= 22)){
 			$_SESSION['zodiac'] = "VIRGO";
 			echo "Virgo";
 			header("Location: ../views/view-zodiac.php");
 		}

 		//case for input date for September exceeding 30
 		else if($month == 9 && $date >30){
 			header("Location: ".$_SERVER['HTTP_REFERER']);
 		}

 		//case for libra
 		else if(($month == 9 && $date >= 23) || ($month == 10 && $date <= 22)){
 			$_SESSION['zodiac'] = "LIBRA";
 			echo "Libra";
 			header("Location: ../views/view-zodiac.php");
 		}


 		//case for scorpio
 		else if(($month == 10 && $date >= 23) || ($month == 11 && $date <= 21)){
 			$_SESSION['zodiac'] = "SCORPIO";
 			echo "Scorpio";
 			header("Location: ../views/view-zodiac.php");
 		}

 		//case for input date for November exceeding 30
 		else if($month == 11 && $date >30){
 			header("Location: ".$_SERVER['HTTP_REFERER']);
 		}

 		//case for sagitarrius
 		else if(($month == 11 && $date >= 22) || ($month == 12 && $date <= 21)){
 			$_SESSION['zodiac'] = "SAGITTARIUS";
 			echo "Sagittarius";
 			header("Location: ../views/view-zodiac.php");
 		}

 		//case for capricorn
 		else if(($month == 12 && $date >= 22) || ($month == 1 && $date <= 19)){
 			$_SESSION['zodiac'] = "CAPRICORN";
 			echo "Capricorn";
 			header("Location: ../views/view-zodiac.php");
 		}

 		//case invalid date
 		else{
 			header("Location: ".$_SERVER['HTTP_REFERER']);
 		}
 	}
 ?>