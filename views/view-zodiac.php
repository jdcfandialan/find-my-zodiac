<?php 
	session_start();
 ?>

<!DOCTYPE html>
<html>
<head>
	<title>Find my Zodiac Sign</title>
	<link rel="stylesheet" type="text/css" href="../assets/styles/style.css">
</head>
<body>
	<div class="d-flex justify-content-center align-items-center vh-100 flex-column">
		<h1>Hello <?php echo $_SESSION['name'] ?>!</h1>
		<h1>You are a <?php echo $_SESSION['zodiac'] ?>.</h1>
	</div>

	<link rel="stylesheet" type="text/css" href="https://bootswatch.com/4/slate/bootstrap.css">
</body>
</html>